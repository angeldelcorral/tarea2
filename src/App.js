import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import PokemonsContextProvider from "./contexts/PokemonsContext";  
import Pokedex from "./components/Pokedexs";
import Header from "./components/Common/Header";
import NotFound from "./components/NotFound";
import "./App.css";

const App = () => (
  <BrowserRouter>
    <Header />
    <Switch>
      <Route exact path="/">
        <PokemonsContextProvider>
          <Pokedex />
        </PokemonsContextProvider>
      </Route>
      <Route component={NotFound} />
    </Switch>
  </BrowserRouter>
);

export default App;
