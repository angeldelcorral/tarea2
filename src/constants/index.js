const base_url = "https://pokeapi.co/api/v2/";
const img_url =
  "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/";

export const getDataList = (offset, limit) =>
  `${base_url}pokemon?offset=${offset}&limit=${limit}`;

export const getImage = (id) => `${img_url}${id}.png`;
