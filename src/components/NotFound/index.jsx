import React from "react";
import { Link } from "react-router-dom";
import { Container, Button } from "react-bootstrap";
import logoImage from "../../assets/images/pokeball.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";

const NotFound = () => {
  return (
    <Container>
      <div className="error">
        <img className="error-img" src={logoImage} alt="Error" />
        <p className="error-text">Sorry, an error has ocurred. Try again.</p>
        <Link to="/">
          <Button>
            <FontAwesomeIcon icon={faChevronLeft}></FontAwesomeIcon> Back
          </Button>
        </Link>
      </div>
    </Container>
  );
};

export default NotFound;
