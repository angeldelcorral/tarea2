import React from "react";
import { Container, Row, Card } from "react-bootstrap";

const Pokemons = ({ datos }) => {
  return (
    <>
      <Container className="center-h">
        <Row xs={1} md={2} className="g-3">
          {datos.length
            ? datos.map((dato, key = dato.url) => (
                <Card style={{ width: "10rem" }} key={key}>
                  <Card.Img variant="top" src={dato.img_url} />
                  <Card.Body>
                    <Card.Title>{dato.name}</Card.Title>
                  </Card.Body>
                </Card>
              ))
            : ""}
        </Row>
      </Container>
    </>
  );
};

export default Pokemons;
