import React, { useContext } from "react";
import { PokemonContext } from "../../contexts/PokemonsContext";
import Loader from "../Common/Loader";
import Pagination from "../Common/Pagination";
import Pokemons from "./Pokemons";

const Pokedex = () => {
  const {
    showLoader,
    datos,
    setOffset,
    offset,
    limit,
    setPagNumber,
    pagNumber,
    totalPags,
    disabledForwardBtn,
    disabledBackwardBtn,
    getListaDatos,
  } = useContext(PokemonContext);

  return (
    <>
      {showLoader ? (
        <Loader width={"50px"} />
      ) : (
        <>
          <Pagination
            setOffset={setOffset}
            setPagNumber={setPagNumber}
            offset={offset}
            limit={limit}
            pagNumber={pagNumber}
            totalPags={totalPags}
            disabledBackwardBtn={disabledBackwardBtn}
            disabledForwardBtn={disabledForwardBtn}
            getListaDatos={getListaDatos}
          ></Pagination>

          <Pokemons datos={datos}></Pokemons>
        </>
      )}
    </>
  );
};

export default Pokedex;
