import React from "react";
import { Link } from "react-router-dom";
import { Navbar, Nav, Container } from "react-bootstrap";
import logoImage from "../../assets/images/pokeball.png";

const Header = () => {
  return (
    <>
      <Navbar bg="dark" variant="dark" sticky="top">
        <Container>
          <Navbar.Brand>
            {" "}
            <img src={logoImage} alt="Pokedex" width="24px" /> Pokedex
          </Navbar.Brand>
          <Nav className="me-auto">
            <Nav.Link
              href="https://www.linkedin.com/in/angel-del-corral/"
              target="_blanck"
            >
              Made by <strong> Angel Del Corral</strong>
            </Nav.Link>
          </Nav>
        </Container>
      </Navbar>
    </>
  );
};

export default Header;
