import React from "react";
import { Container, Button } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBackward, faForward } from "@fortawesome/free-solid-svg-icons";

const Pagination = ({
  setOffset,
  setPagNumber,
  totalPags,
  offset,
  limit,
  pagNumber,
  disabledBackwardBtn,
  disabledForwardBtn,
  getListaDatos,
}) => {
  const handleForward = () => {
    let newValue = 0;
    newValue = offset + limit;
    setOffset(newValue);
    setPagNumber(pagNumber + 1);
    getListaDatos(offset, limit);
  };

  const handleBackward = () => {
    let newValue = 0;
    newValue = offset - limit;
    setOffset(newValue);
    setPagNumber(pagNumber - 1);
    getListaDatos(offset, limit);
  };
  return (
    <>
      <Container className="paginator-h">
        <div className="paginator">
          <Button onClick={handleBackward} disabled={disabledBackwardBtn}>
            <FontAwesomeIcon icon={faBackward} /> Previous
          </Button>{" "}
          Pag {pagNumber} / {totalPags}{" "}
          <Button onClick={handleForward} disabled={disabledForwardBtn}>
            Next <FontAwesomeIcon icon={faForward} />
          </Button>
        </div>
      </Container>
    </>
  );
};

export default Pagination;
