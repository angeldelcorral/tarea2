import React from "react";
import imgLoader from "../../assets/images/pokeball.png";

const Loader = ({ width }) => {
  return (
    <div className="center-v">
      <img
        src={imgLoader}
        alt="Loading...."
        className="loader-img"
        width={width}
      />
      &nbsp;&nbsp;&nbsp;
      <span className="center-h"> Loading...</span>
    </div>
  );
};

export default Loader;
