import React, { createContext, useState, useEffect } from "react";
import { getDataList, getImage } from "../constants";

export const PokemonContext = createContext();

const PokemonsContextProvider = ({ children }) => {
  const [datos, setDatos] = useState([]);
  const [offset, setOffset] = useState(0);
  const [showLoader, setShowLoader] = useState(false);
  const [totalDatos, setTotalDatos] = useState(0);
  const [disabledForwardBtn, setDisabledForwardBtn] = useState(false);
  const [disabledBackwardBtn, setDisabledBackwardBtn] = useState(true);
  const [pagNumber, setPagNumber] = useState(1);
  const [totalPags, setTotalPags] = useState(0);

  const limit = 100;

  const getListaDatos = () => {
    setShowLoader(true);
    {
      offset === 0
        ? setDisabledBackwardBtn(true)
        : setDisabledBackwardBtn(false);
    }
    {
      offset >= totalDatos && totalDatos > 0
        ? setDisabledForwardBtn(true)
        : setDisabledForwardBtn(false);
    }

    const getDataUrl = getDataList(offset, limit);
    fetch(getDataUrl)
      .then((res) => res.json())
      .then((data) => {
        const result = data.results.map((item = item.url
          .slice(0, -1)
          .split("/")[item.url.slice(0, -1).split("/").length - 1]) => ({
          name: item.name,
          id: item.url.slice(0, -1).split("/")[
            item.url.slice(0, -1).split("/").length - 1
          ],
          img_url: getImage(
            item.url.slice(0, -1).split("/")[
              item.url.slice(0, -1).split("/").length - 1
            ]
          ),
        }));
        setDatos(result);
        setTotalDatos(parseInt(data.count));
        setTotalPags(Math.ceil(parseInt(data.count) / limit));
        setShowLoader(false);
      })
      .catch((error) => console.log(error));
  };

  useEffect(() => {
    getListaDatos();
  }, [offset]);

  //   Use Providers
  return (
    <PokemonContext.Provider
      value={{
        showLoader,
        datos,
        setOffset,
        offset,
        limit,
        setPagNumber,
        pagNumber,
        totalPags,
        disabledForwardBtn,
        disabledBackwardBtn,
        getListaDatos,
      }}
    >
      {children}
    </PokemonContext.Provider>
  );
};

export default PokemonsContextProvider;
